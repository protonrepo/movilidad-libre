# Movilidad Libre
Análisis del estado de los dispositivos móviles en el mundo del software libre.

###### Licencia
[Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/)
